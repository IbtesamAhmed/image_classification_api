import pandas as pd
import numpy as np
import pickle
import os
import flask
import json
from flask import Flask, render_template,request,url_for
import boto
from boto.s3.key import Key
from keras.models import load_model
from keras.applications.inception_resnet_v2 import preprocess_input
from keras.preprocessing import image
from keras.models import Model
from keras.layers import Input
import cv2
from PIL import Image
import requests
from io import BytesIO
np.random.seed(90)
import tensorflow as tf
import urllib.parse

def load_files():
    global inception
    global classifier

    directory = "pretrained"
    print("Loading Inception....")

    s3_dir = directory+"/"+"inception.h5"
    local_dir = directory
    local_path = local_dir+"/"+"inception.h5"
    print(local_path)
    if not os.path.exists(local_path):
        print("file %s not found.... downloading." % local_path)
        key_obj.key = s3_dir
        print(key_obj.key)
        os.makedirs(local_dir, exist_ok=True)
        contents = key_obj.get_contents_to_filename(local_path)
    inception = load_model(local_path)
    global graph
    graph = tf.get_default_graph()
    #print(inception.summary())
    # with open(local_path, 'rb') as handle:
    #     vectorizer = pickle.load(handle)
    # print("vectorizer type :",type(vectorizer))


    directory = "classifier"
    print("loading the classifier")

    s3_dir = directory+"/"+"classifier.pickle"
    local_dir = directory
    local_path = local_dir+"/"+"classifier.pickle"
    if not os.path.exists(local_path):
        print("file %s not found.... downloading." % local_path)
        key_obj.key = s3_dir
        print(key_obj.key)
        os.makedirs(local_dir, exist_ok=True)
        contents = key_obj.get_contents_to_filename(local_path)
    classifier = pickle.load(open(local_path, 'rb'))


def testing(url):
    global response
    response = requests.get(url)
    img = Image.open(BytesIO(response.content))


#     f_name = 'templates/img{}'.format(f_ext)
#     with open(f_name, 'wb') as f:
#         f.write(response.content)

    x = image.img_to_array(img)
    x = cv2.resize(x,(299,299))
    #
    if (len(x.shape) !=3 ):
        x = cv2.cvtColor(x,cv2.COLOR_GRAY2RGB)
    if len(x.shape) > 2 and x.shape[2] == 4:
        x = cv2.cvtColor(x, cv2.COLOR_BGRA2BGR)


    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)
    print(x.shape)
    with graph.as_default():
           feature = inception.predict(x)
    #feature = inception.predict(x)
    flat = feature.flatten()
    flat= np.expand_dims(flat, axis=0)
    preds = classifier.predict(flat)
    final_prediction=preds[0]
    prob=max(classifier.predict_proba(flat)[0])
    if((final_prediction=='nudity') & (prob>0.9)):
        final_prediction='nudity'
    elif((final_prediction=='violence') & (prob> 0.87)):
        final_prediction=='violence'
    else:
        final_prediction='safe'
    print (final_prediction)

    return final_prediction



# stop_words=set(stopwords.words('english'))
# wordnet_lemmatizer = WordNetLemmatizer()

app = Flask(__name__)


@app.route('/ic')
def index():
	return render_template('index.html')



@app.route('/predict', methods =['POST'])
def predict():
    global rawtext
    failedResponse = json.dumps({
        "success" : "False",
        "error": "Unable to process data",
    })

    if request.method == 'POST':
        rawtext = request.form['rawtext']
        print(rawtext)
        try:
            print("Entered in testing")
            z = testing(rawtext)
            print(z)
            return render_template('index.html',predicted_class = z)
        except Exception as e:
            z = "Wrong combination"
            print(e)
            return render_template('index.html',predicted_class = failedResponse)




if __name__ == '__main__':
    print("*Loading Necessary Files And starting server")
    with open('config.json') as f:

        print("doneeee")
        config = json.load(f)
        AWS_S3_HOST = config['AWS_S3_HOST']
        BUCKET_NAME = config['BUCKET_NAME']
        ACCESS_KEY = config['ACCESS_KEY']
        SECRET_KEY = config['SECRET_KEY']
        s3_path = config['s3_path']
        version = config['version']
        port = config['port']
        host = config['host']



    conn = boto.connect_s3(ACCESS_KEY, SECRET_KEY, host=AWS_S3_HOST)
    bucket = conn.get_bucket(BUCKET_NAME)
    key_obj = Key(bucket)

    load_files()

    app.run(host=host, port=port, threaded=True)
